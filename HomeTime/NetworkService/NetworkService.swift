//
//  NetworkService.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A network service used to make url requests with different http methods and for different endpoints
class NetworkService: NetworkServiceProtocol {
    private let urlSession: URLSession

    /// To init a network service instance with a url session configuration set by default
    /// - Parameter configuration: a url session configuration to be injected
    init(configuration: URLSessionConfiguration = .default) {
        self.urlSession = URLSession(configuration: configuration)
    }

    /// This method constructs a URL from endpoint and parameters
    /// - Parameter endpoint: a standard enpoint
    /// - Parameter parameters: parameters used for url query or http body
    private func constructURL(endpoint: EndpointProtocol, parameters: Parameters?) -> URL? {
        guard let url = endpoint.url else { return nil }
        guard let parameters = parameters, endpoint.httpMethod == .get else {
            return url
        }

        var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true)
        var queryItems = urlComponents?.queryItems ?? []

        queryItems += parameters.map { URLQueryItem(name: $0.key, value: $0.value) }
        urlComponents?.queryItems = queryItems

        return urlComponents?.url
    }

    /// This method constructs a URLRequest from endpoint and parameters
    /// - Parameter endpoint: a standard enpoint
    /// - Parameter parameters: parameters used for url query or http body
    private func constructURLRequest(endpoint: EndpointProtocol, parameters: Parameters?) throws -> URLRequest? {
        guard let url = constructURL(endpoint: endpoint, parameters: parameters) else {
            return nil
        }

        var request = URLRequest(url: url)
        request.httpMethod = endpoint.httpMethod.rawValue
        request.allHTTPHeaderFields = endpoint.httpHeaders

        if let parameters = parameters, [HTTPMethod.post, .put, .patch, .delete].contains(endpoint.httpMethod) {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        }

        return request
    }

    /// Perform a url request by making a data task
    /// - Parameter endpoint: a standard endpoint which contains http method, url and other needed infomation
    /// - Parameter parameters: parameters used for url query or http body
    /// - Parameter completion: a result which could be either success with a response or failure with an error
    private func performRequest(endpoint: EndpointProtocol,
                                parameters: Parameters?,
                                completion: @escaping (_ result: Result<APIClientResponse, APIClientError>) -> Void) {
        guard let urlRequest = try? constructURLRequest(endpoint: endpoint, parameters: parameters) else {
            completion(.failure(.invalidURL(endpoint: endpoint)))
            return
        }

        let dataTask = urlSession.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                completion(.failure(.general(message: error.localizedDescription)))
                return
            }

            guard let httpResponse = response as? HTTPURLResponse else {
                completion(.failure(.invalidResponse(response)))
                return
            }

            switch httpResponse.statusCode {
            case 200..<300,
                 300..<400:
                completion(.success(APIClientResponse(httpStatusCode: httpResponse.statusCode, data: data)))
            case 400..<500:
                completion(.failure(.httpClientError(statusCode: httpResponse.statusCode)))
            case 500...:
                completion(.failure(.httpServerError(statusCode: httpResponse.statusCode)))
            default:
                completion(.failure(.unknownStatusCode(httpResponse.statusCode)))
            }
        }
        dataTask.resume()
    }

    /// Perform a url request by making a data task
    /// - Parameter endpoint: a standard endpoint which contains http method, url and other needed infomation
    /// - Parameter parameters: parameters used for url query or http body
    /// - Parameter completion: a result which could be either success with a decodable model or failure with an error
    private func performRequest<T: Decodable>(endpoint: EndpointProtocol,
                                              parameters: Parameters?,
                                              completion: @escaping (Result<T, APIClientError>) -> Void)  {
        performRequest(endpoint: endpoint, parameters: parameters) { result in
            if case .failure(let error) = result {
                completion(.failure(error))
                return
            }

            guard let data = try? result.get().data else {
                completion(.failure(.emptyResponse(endpoint: endpoint)))
                return
            }

            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                completion(.success(model))
            } catch {
                completion(.failure(.dataCorrupted(endpoint: endpoint)))
            }
        }
    }

    /// Perform a url request by making a data task
    /// - Parameter endpoint: a standard endpoint which contains http method, url and other needed infomation
    /// - Parameter parameters: parameters used for url query or http body
    /// - Parameter success: a success completion with a decodable model
    /// - Parameter failure: a failure completion  with an error
    func performRequest<T: Decodable>(endpoint: EndpointProtocol,
                                      parameters: Parameters?,
                                      success: ((T) -> Void)?,
                                      failure: ((APIClientError) -> Void)?)  {
        performRequest(endpoint: endpoint, parameters: parameters) { (result: Result<T, APIClientError>) in
            DispatchQueue.main.async {
                switch result {
                case .success(let model):
                    success?(model)
                case .failure(let error):
                    failure?(error)
                }
            }
        }
    }
}
