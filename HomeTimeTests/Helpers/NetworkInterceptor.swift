//
//  NetworkInterceptor.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A network interceptor conforming to `URLProtocol` to change the api response. After setting up hooks,
/// this will intercept the response and update its properties with the mock response configured.
class NetworkInterceptor: URLProtocol {
    struct MockResponse {
        var responseData: Data?
        var responseHeader: [String: String]
        var responseStatusCode: Int
        var responseError: NSError?
        var waitingTime: TimeInterval
    }

    static fileprivate var hooks: [String: MockResponse] = [:]

    static func setHook(urlString: String,
                        responseJSON: Any,
                        responseHeader: [String: String] = ["Content-Type": "application/json"],
                        responseStatusCode: Int = 200,
                        responseError: NSError? = nil,
                        waitingTime: TimeInterval = 0.0) {
        let responseData = try? JSONSerialization.data(withJSONObject: responseJSON, options: [])
        let mockResponse = MockResponse(responseData: responseData,
                                        responseHeader: responseHeader,
                                        responseStatusCode: responseStatusCode,
                                        responseError: responseError,
                                        waitingTime: waitingTime)
        hooks[urlString] = mockResponse

    }
}

// MARK: - NSURLProtocol implementation

extension NetworkInterceptor {
    override class func canInit(with request: URLRequest) -> Bool {
        guard let urlString = request.url?.absoluteString else { return false }
        return NetworkInterceptor.hooks.keys.contains(urlString)
    }

    override func startLoading() {
        guard let urlString = request.url?.absoluteString,
            let mockResponse = NetworkInterceptor.hooks[urlString] else { return }

        guard let response = HTTPURLResponse(url: request.url!,
                                             statusCode: mockResponse.responseStatusCode,
                                             httpVersion: "HTTP/1.1",
                                             headerFields: mockResponse.responseHeader) else { return }

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + DispatchTimeInterval.microseconds(Int(mockResponse.waitingTime * 1000_000))) {
            self.client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: URLCache.StoragePolicy.notAllowed)
            if let data = mockResponse.responseData {
                self.client?.urlProtocol(self, didLoad: data)
                self.client?.urlProtocolDidFinishLoading(self)
            } else if let error = mockResponse.responseError {
                self.client?.urlProtocol(self, didFailWithError: error)
            }
        }
    }

    override class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    override func stopLoading() {
        let error = NSError(domain: "NetworkInterceptor", code: 0, userInfo: [NSLocalizedDescriptionKey: "The request is canceled!"])
        client?.urlProtocol(self, didFailWithError: error)
    }
}
