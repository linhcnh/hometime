//
//  Decodable+Initialisers.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A extension with handy init methods for initialising a decodable model from json string or data
extension Decodable {
    init?(json: String) {
        let data = json.data(using: .utf8)!
        self.init(data: data)
    }

    init?(data: Data) {
        do {
            self = try JSONDecoder().decode(Self.self, from: data)
        } catch {            
            assertionFailure(error.localizedDescription)
            return nil
        }
    }
}
