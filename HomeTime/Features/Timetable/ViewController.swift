//
//  Copyright © 2017 REA. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    // MARK: - IBOutlets

    @IBOutlet var tramTimesTable: UITableView!

    // MARK: - Properties

    private var viewModel: ViewModel!

    // MARK: - ViewController

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchContent()
    }

    @IBAction func clearButtonTapped(_ sender: UIBarButtonItem) {
        clearContent()
    }

    @IBAction func loadButtonTapped(_ sender: UIBarButtonItem) {
        clearContent()
        fetchContent()
    }

    // MARK: - Private methods

    private func clearContent() {
        viewModel.clearTimetable()
        tramTimesTable.reloadData()
    }

    private func fetchContent() {
        viewModel.getTrams { [weak self] error in
            if let error = error {
                self?.handleError(error)
            }

            self?.tramTimesTable.reloadData()
        }
    }

    private func handleError(_ error: APIClientError) {
        let alertController = UIAlertController(title: viewModel.alertControllerTitle,
                                                message: error.errorDescription,
                                                preferredStyle: .alert)
        let alertAction = UIAlertAction(title: viewModel.alertActionTitle,
                                        style: .default,
                                        handler: nil)
        alertController.addAction(alertAction)

        present(alertController, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource

extension ViewController {
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: viewModel.cellIdentifier, for: indexPath)
        cell.textLabel?.text = viewModel.tramInfoForItem(at: indexPath)

        return cell
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.timetable.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.tramsForItems(at: section)?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForItems(at: section)
    }
}

// MARK: - StoryboardCreatable

extension ViewController: StoryboardCreatable {
    static func instantiate(viewModel: ViewModel) -> ViewController {
        let viewController = ViewController.instantiateFromStoryboard()
        viewController.viewModel = viewModel
        return viewController
    }
}
