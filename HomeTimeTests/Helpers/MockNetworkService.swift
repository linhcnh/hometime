//
//  MockNetworkService.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation
@testable import HomeTime

/// A network service intended for use in unit tests. It can be given fixed values to return.
/// These values can be modified on the fly to change the response, though this is not recommended.
/// As with all mock network services, it can also be made to emit errors in response to any calls.
class MockNetworkService: NetworkServiceProtocol {
    typealias MockEndpointResponseObjects = [URL: Any?]

    private let mockEndpointResponseObjects: MockEndpointResponseObjects?

    init(mockEndpointResponseObjects: MockEndpointResponseObjects? = nil) {
        self.mockEndpointResponseObjects = mockEndpointResponseObjects
    }

    func performRequest<T>(endpoint: EndpointProtocol,
                           parameters: Parameters?,
                           success: ((T) -> Void)?,
                           failure: ((APIClientError) -> Void)?) where T : Decodable {

        guard let model = mockEndpointResponseObjects?[endpoint.url!] as? T else {
            failure?(.general(message: "Mock model not found"))
            return
        }

        success?(model)
    }
}
