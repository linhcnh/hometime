//
//  NetworkServiceProtocol.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A standard network service protocol used to perform url requests with different http methods and for different endpoints
protocol NetworkServiceProtocol {
    func performRequest<T: Decodable>(endpoint: EndpointProtocol,
                                      parameters: Parameters?,
                                      success: ((T) -> Void)?,
                                      failure: ((APIClientError) -> Void)?)
}
