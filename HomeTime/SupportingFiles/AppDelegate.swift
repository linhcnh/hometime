//
//  Copyright © 2017 REA. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    private var viewController: ViewController {
        let networkService = NetworkService()
        let authManager = AuthManager(networkService: networkService)
        let dateConverter = DotNetDateConverter()
        let viewModel = ViewModel(networkService: networkService, authManager: authManager, dateConverter: dateConverter)
        let viewController = ViewController.instantiate(viewModel: viewModel)

        return viewController
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let navigationController = UINavigationController(rootViewController: viewController)

        window = UIWindow()
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()

        return true
    }

}
