//
//  StoryboardCreatable.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

/// A storyboard creatable protocol used to instantiate a view controller from a storyboard assuming that
/// each storyboard contains one view controller that has the name matching with storyboard name
public protocol StoryboardCreatable: class {
    static var storyboardName: String { get }
    static func instantiateFromStoryboard() -> Self
}

// MARK: - Handy methods returning a view controller matching with the current storyboard name

public extension StoryboardCreatable {
    static var storyboardName: String {
        return String(describing: self)
    }

    static func instantiateFromStoryboard() -> Self {
        return instantiateFromStoryboard(name: storyboardName)
    }

    static func instantiateFromStoryboard(name: String) -> Self {
        return UIStoryboard(name: name, bundle: Bundle(for: Self.self)).instantiateInitialViewController() as! Self
    }
}
