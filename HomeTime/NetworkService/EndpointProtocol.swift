//
//  EndpointProtocol.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A protocol which should be conformed by all endpoints
protocol EndpointProtocol {
    var name: String? { get }
    var scheme: String { get }
    var host: String { get }
    var httpMethod: HTTPMethod { get }
    var path: String { get }
    var httpHeaders: HTTPHeaders? { get }
}

//
// MARK: - Constructed URL
//
extension EndpointProtocol {
    var url: URL? {
        return URL(string: path, relativeTo: URL(string: "\(scheme)\(host)"))
    }
}
