//
//  MockEndpoint.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation
@testable import HomeTime

/// A mock endpoint conforming to endpoint protocol can be used for testing support
class MockEndpoint: EndpointProtocol {
    let name: String?
    let httpMethod: HTTPMethod
    let path: String

    var scheme: String {
        return "http://"
    }

    var host: String {
        return "www.apiclient.com"
    }

    var httpHeaders: HTTPHeaders? {
        return nil
    }

    private init(name: String? = #function, httpMethod: HTTPMethod, path: String) {
        self.name = name
        self.httpMethod = httpMethod
        self.path = path
    }
}

extension MockEndpoint {
    static var mock: EndpointProtocol {
        return MockEndpoint(
            httpMethod: .get,
            path: "/mock"
        )
    }
}
