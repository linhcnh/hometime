//
//  TramResponseModel.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A standard response model for requesting tram routes
struct TramResponseModel: Decodable {
    let errorMessage: String?
    let hasError: Bool
    let hasResponse: Bool
    let responseObject: [TramModel]
}

/// A model containing tram route details
struct TramModel: Decodable {
    let destination: String
    let predictedArrivalDateTime: String
    let routeNo: String

    private enum CodingKeys: String, CodingKey {
        case destination = "Destination"
        case predictedArrivalDateTime = "PredictedArrivalDateTime"
        case routeNo = "RouteNo"
    }
}
