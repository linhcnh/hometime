//
//  TokenResponseModel.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A standard response model for requesting tokens
struct TokenResponseModel: Decodable {
    let errorMessage: String?
    let hasError: Bool
    let hasResponse: Bool
    let responseObject: [TokenModel]
}

/// A  model containing device token
struct TokenModel: Decodable {
    let deviceToken: String

    private enum CodingKeys: String, CodingKey {
        case deviceToken = "DeviceToken"
    }
}
