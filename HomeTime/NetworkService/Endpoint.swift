//
//  Endpoint.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// An endpoint structure conforming to the standard endpoint protocol
struct Endpoint: EndpointProtocol {
    let name: String?
    let httpMethod: HTTPMethod
    let path: String

    var scheme: String {
        return "http://"
    }

    var host: String {
        return "ws3.tramtracker.com.au"
    }

    var httpHeaders: HTTPHeaders? {
        return ["Content-Type": "application/json", "Accept": "application/json"]
    }

    /// Privately init an enpoint instance
    /// - Parameter name: endpoint function name
    /// - Parameter httpMethod: standard http method
    /// - Parameter path: path to the resouce api which is used to construct an url
    /// - Parameter httpHeaders: http headers which is optional
    private init(name: String? = #function, httpMethod: HTTPMethod, path: String) {
        self.name = name
        self.httpMethod = httpMethod
        self.path = path
    }
}

// MARK: - endpoints to request token and trams 

extension Endpoint {
    static var token: EndpointProtocol {
        return Endpoint(
            httpMethod: .get,
            path: "/TramTracker/RestService/GetDeviceToken"
        )
    }

    static func trams(stopId: String) -> EndpointProtocol {
        return Endpoint(
            httpMethod: .get,
            path: "/TramTracker/RestService/GetNextPredictedRoutesCollection/\(stopId)/78/false"
        )
    }
}
