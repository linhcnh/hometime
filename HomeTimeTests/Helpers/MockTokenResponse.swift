//
//  MockTokenResponse.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

// MARK: - MockTokenResponse

enum MockTokenResponse: String {
    case all

    var mock: String {
        switch self {
        case .all: return all
        }
    }
}

// MARK: - All response

extension MockTokenResponse {
    private var all: String {
        return #"""
        {
            "errorMessage": null,
            "hasError": false,
            "hasResponse": true,
            "responseObject": [
                {
                    "__type": "AddDeviceTokenInfo",
                    "DeviceToken": "208c0c2f-139f-427d-bc99-cb3fd44eff3e"
                }
            ],
            "timeRequested": "/Date(1574828831566+1100)/",
            "timeResponded": "/Date(1574828831577+1100)/",
            "webMethodCalled": "GetDeviceToken"
        }
        """#
    }
}
