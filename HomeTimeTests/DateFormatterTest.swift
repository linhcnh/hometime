//
//  Copyright (c) 2015 REA. All rights reserved.
//

import UIKit
import XCTest
@testable import HomeTime

class DateFormatterTest: XCTestCase {
  func testShouldConvertTimeIntervalToDateString() {
    let converter = DotNetDateConverter()
    let from = converter.dateFromDotNetFormattedDateString("/Date(1426821000000+1100)/")!
    let result = converter.formattedDateFromString("/Date(1426821588000+1100)/", distanceFrom: from)
    XCTAssertEqual(result, "14:19 (9 mins)")
  }
}
