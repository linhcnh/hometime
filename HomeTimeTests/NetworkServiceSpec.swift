//
//  NetworkServiceSpec.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import HomeTime

class MockURLSessionDataTask: URLSessionDataTask {
    override func resume() {}
}

class MockURLSession: URLSession {
    override func dataTask(with url: URL, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let urlRequest = URLRequest(url: url)
        return dataTask(with: urlRequest, completionHandler: completionHandler)
    }

    override func dataTask(with request: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        let jsonString = """
                        {
                            "response" : "Mock object"
                        }
                        """
        let data = jsonString.data(using: .utf8)!
        completionHandler(data, nil, nil)

        return MockURLSessionDataTask()
    }
}



struct MockResponseModel: Decodable {
    let title: String
}

class NetworkServiceSpec: QuickSpec {
      override func spec() {
        describe("NetworkService") {
            let sampleJSON = ["title": "Hello World"]
            var networkService: NetworkServiceProtocol!

            beforeEach {
                let configuration = URLSessionConfiguration.default
                configuration.protocolClasses?.insert(NetworkInterceptor.self, at: 0)
                networkService = NetworkService(configuration: configuration)
            }

            it("should return a mock response object") {
                NetworkInterceptor.setHook(urlString: MockEndpoint.mock.url!.absoluteString,
                                           responseJSON: sampleJSON)

                waitUntil(timeout: 2) { done in
                    let success: (MockResponseModel) -> Void = { model in
                        expect(model.title).to(equal("Hello World"))
                        done()
                    }

                    let failure: (APIClientError) -> Void = { _ in}

                    networkService.performRequest(endpoint: MockEndpoint.mock,
                                                  parameters: nil,
                                                  success: success,
                                                  failure: failure)

                }
            }

            it("should fail for an empty Response") {
                let expectedErrorMessage = APIClientError.httpClientError(statusCode: 401).errorDescription
                NetworkInterceptor.setHook(urlString: MockEndpoint.mock.url!.absoluteString,
                                           responseJSON: sampleJSON,
                                           responseStatusCode: 401)

                waitUntil(timeout: 2) { done in
                    let success: (MockResponseModel) -> Void = { _ in }

                    let failure: (APIClientError) -> Void = { error in
                        expect(error.errorDescription).to(equal(expectedErrorMessage))
                        done()
                    }

                    networkService.performRequest(endpoint: MockEndpoint.mock,
                                                  parameters: nil,
                                                  success: success,
                                                  failure: failure)
                }

            }
        }
    }
}
