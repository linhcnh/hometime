//
//  APIClientResponse.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// An api client response includes http status code and returned data
struct APIClientResponse {
    let httpStatusCode: Int
    let data: Data?
}
