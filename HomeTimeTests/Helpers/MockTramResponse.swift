//
//  MockTramResponse.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

// MARK: - MockTramResponse

enum MockTramResponse: String {
    case all

    var mock: String {
        switch self {
        case .all: return all
        }
    }
}

// MARK: - All response

extension MockTramResponse {
    private var all: String {
        return #"""
        {
            "errorMessage": null,
            "hasError": false,
            "hasResponse": true,
            "responseObject": [{
                "__type": "NextPredictedRoutesCollectionInfo",
                "AirConditioned": false,
                "Destination": "Balaclava",
                "DisplayAC": false,
                "DisruptionMessage": {
                    "DisplayType": "Text",
                    "MessageCount": 0,
                    "Messages": []
                },
                "HasDisruption": false,
                "HasSpecialEvent": true,
                "HeadBoardRouteNo": "78",
                "InternalRouteNo": 78,
                "IsLowFloorTram": false,
                "IsTTAvailable": true,
                "PredictedArrivalDateTime": "\/Date(1574830524000+1100)\/",
                "RouteNo": "78",
                "SpecialEventMessage": "Industrial action Thurs 10am-2pm: No trams run. Disruptions to 3.30pm as we restore timetables. Consider alternatives",
                "TripID": 0,
                "VehicleNo": 256
            }, {
                "__type": "NextPredictedRoutesCollectionInfo",
                "AirConditioned": false,
                "Destination": "Balaclava",
                "DisplayAC": false,
                "DisruptionMessage": {
                    "DisplayType": "Text",
                    "MessageCount": 0,
                    "Messages": []
                },
                "HasDisruption": false,
                "HasSpecialEvent": true,
                "HeadBoardRouteNo": "78",
                "InternalRouteNo": 78,
                "IsLowFloorTram": false,
                "IsTTAvailable": true,
                "PredictedArrivalDateTime": "\/Date(1574831220000+1100)\/",
                "RouteNo": "78",
                "SpecialEventMessage": "Industrial action Thurs 10am-2pm: No trams run. Disruptions to 3.30pm as we restore timetables. Consider alternatives",
                "TripID": 0,
                "VehicleNo": 266
            }, {
                "__type": "NextPredictedRoutesCollectionInfo",
                "AirConditioned": false,
                "Destination": "Balaclava",
                "DisplayAC": false,
                "DisruptionMessage": {
                    "DisplayType": "Text",
                    "MessageCount": 0,
                    "Messages": []
                },
                "HasDisruption": false,
                "HasSpecialEvent": true,
                "HeadBoardRouteNo": "78",
                "InternalRouteNo": 78,
                "IsLowFloorTram": false,
                "IsTTAvailable": true,
                "PredictedArrivalDateTime": "\/Date(1574831940000+1100)\/",
                "RouteNo": "78",
                "SpecialEventMessage": "Industrial action Thurs 10am-2pm: No trams run. Disruptions to 3.30pm as we restore timetables. Consider alternatives",
                "TripID": 0,
                "VehicleNo": 0
            }],
            "timeRequested": "\/Date(1574830474051+1100)\/",
            "timeResponded": "\/Date(1574830474136+1100)\/",
            "webMethodCalled": "GetNextPredictedRoutesCollection"
        }
        """#
    }
}
