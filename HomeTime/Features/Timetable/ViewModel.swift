//
//  ViewModel.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

class ViewModel {
    typealias Timetable = [Direction: [TramModel]]

    private let networkService: NetworkServiceProtocol
    private let authManager: AuthManager
    private let dateConverter: DotNetDateConverter
    private let noTramMessage = "No upcoming trams. Tap load to fetch"

    private(set) var timetable: Timetable = [:]

    let cellIdentifier = "TramCellIdentifier"
    let alertActionTitle = "OK"
    let alertControllerTitle = "Error"

    /// To init with network service, auth manager and date converter injected for requesting token and formatting date string
    /// - Parameter networkService: a network service managing requests and responses to the api
    /// - Parameter authManager: an authentication manager which manage api tokens, ids and dev info
    /// - Parameter dateConverter: a date converter to convert and formate dates
    init(networkService: NetworkServiceProtocol, authManager: AuthManager, dateConverter: DotNetDateConverter) {
        self.networkService = networkService
        self.authManager = authManager
        self.dateConverter = dateConverter
    }

    /// Return formatted tram information associated with a specific index path and the input `fromDate` is used for calculate the time difference
    /// - Parameter indexPath: input index path
    /// - Parameter fromDate: from date which is set to the current date by default
    func tramInfoForItem(at indexPath: IndexPath, with fromDate: Date = Date()) -> String  {
        guard let tram = tramForItem(at: indexPath) else { return noTramMessage }

        let time = dateConverter.formattedDateFromString(tram.predictedArrivalDateTime, distanceFrom: fromDate)
        let info = "Route \(tram.routeNo) to \(tram.destination) at \(time)"

        return info
    }

    /// Return tram data associated with a specific section
    /// - Parameter section: input section
    func tramsForItems(at section: Int) -> [TramModel]? {
        switch section {
        case 0: return timetable[Direction.north]
        case 1: return timetable[Direction.south]
        default: return nil
        }
    }

    /// Return a title associated with a specific section
    /// - Parameter section: input section
    func titleForItems(at section: Int) -> String? {
        switch section {
        case 0: return Direction.north.title
        case 1: return Direction.south.title
        default: return nil
        }
    }

    /// Clear the data resource
    func clearTimetable() {
        timetable.removeAll()
    }

    /// A method to make both north and south tram requests
    /// - Parameter completion: a completion handler which may or may not return an error
    func getTrams(completion: ((APIClientError?) -> Void)?) {
        var responseCount = 0
        var responseError: APIClientError?

        let requestCompletion: (APIClientError?) -> Void = { error in
            responseCount += 1

            if let error = error {
                responseError = error
            }

            if responseCount == 2 {
                completion?(responseError)
            }
        }

        getTrams(stopId: Direction.north.stopId, completion: requestCompletion)
        getTrams(stopId: Direction.south.stopId, completion: requestCompletion)
    }

    /// A method to make tram request based on stop id. If there is no token yet, a token request will be triggered
    /// - Parameter stopId: stop id to be added to the url
    /// - Parameter completion: a completion handler which may or may not return an error
    private func getTrams(stopId: String, completion: ((APIClientError?) -> Void)?) {
        guard let token = authManager.tkn else {
            let success: (String) -> Void = { [weak self] token in
                self?.getTrams(stopId: stopId, token: token, completion: completion)
            }

            let failure: (APIClientError) -> Void = { error in
                completion?(error)
            }

            authManager.requestToken(success: success, failure: failure)
            return
        }

        getTrams(stopId: stopId, token: token, completion: completion)
    }

    /// A method to make tram request based on stop id and parameters
    /// - Parameter stopId: stop id to be added to the url
    /// - Parameter token: token used as a parameter
    /// - Parameter completion: a completion handler which may or may not return an error
    private func getTrams(stopId: String, token: String, completion: ((APIClientError?) -> Void)?) {
        let endpoint = Endpoint.trams(stopId: stopId)
        let parameters = ["aid": authManager.aid,
                          "cid": authManager.cid,
                          "tkn": token]

        let success: (TramResponseModel) -> Void = { [weak self] model in
            guard model.hasError == false else {
                completion?(APIClientError.general(message: model.errorMessage))
                return
            }

            self?.updateTimetable(stopId: stopId, trams: model.responseObject)
            completion?(nil)
        }

        let failure: (APIClientError) -> Void = { error in
            completion?(error)
        }

        networkService.performRequest(endpoint: endpoint,
                                      parameters: parameters,
                                      success: success,
                                      failure: failure)
    }

    /// Update timetable data resouce after retreiving data from the api
    /// - Parameter stopId: stop id as a key of the dictionary data resource
    /// - Parameter trams: tram list as a value of the dictionary data resource
    private func updateTimetable(stopId: String, trams: [TramModel]) {
        guard let direction = Direction.allCases.filter({ $0.stopId == stopId }).first else {
            return
        }
        timetable[direction] = trams
    }

    /// Get tram model for item at a specific index path
    /// - Parameter indexPath: input index path
    private func tramForItem(at indexPath: IndexPath) -> TramModel? {
        guard let trams = tramsForItems(at: indexPath.section),
            trams.indices.contains(indexPath.item) else {
            return nil
        }

        return trams[indexPath.item]
    }
}
