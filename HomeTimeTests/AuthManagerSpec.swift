//
//  AuthManagerSpec.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import HomeTime

class AuthManagerSpec: QuickSpec {
    override func spec() {
        describe("AuthManager") {
            let tokenJson = MockTokenResponse.all.mock
            let tokenResponseModel = TokenResponseModel(json: tokenJson)!

            let mockEndpointResponseObjects = [Endpoint.token.url!: tokenResponseModel] as [URL : Any]
            let networkService = MockNetworkService(mockEndpointResponseObjects: mockEndpointResponseObjects)
            var authManager: AuthManager!

            beforeEach {
                authManager = AuthManager(networkService: networkService)
            }

            context("when initialising the auth manager") {
                it("should match the constant variables") {
                    expect(authManager.aid).to(equal("TTIOSJSON"))
                    expect(authManager.devInfo).to(equal("HomeTimeiOS"))
                    expect(authManager.cid).to(equal("2"))
                }

                it("should have no token") {                    
                    expect(authManager.tkn).to(beNil())
                }
            }

            context("when making a new token request") {
                it("should should receive a new token") {
                    authManager.requestToken(success: nil, failure: nil)
                    expect(authManager.tkn).to(equal("208c0c2f-139f-427d-bc99-cb3fd44eff3e"))
                }
            }
        }
    }
}
