//
//  APIClientError.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// An enum with all standardised errors
enum APIClientError: LocalizedError {
    case general(message: String?)
    case httpClientError(statusCode: Int)
    case httpServerError(statusCode: Int)
    case invalidURL(endpoint: EndpointProtocol)
    case invalidResponse(URLResponse?)
    case emptyResponse(endpoint: EndpointProtocol)
    case dataCorrupted(endpoint: EndpointProtocol)
    case unknownStatusCode(Int)

    var errorDescription: String? {
        switch self {
        case let .general(message): return message ?? "An unknown error occured requesting data from the API."
        case let .httpClientError(statusCode): return "Client error occurred: \(statusCode)"
        case let .httpServerError(statusCode): return "Server error occurred: \(statusCode)"
        case let .invalidURL(endpoint): return "Unable to construct url for endpoint \(endpoint.name ?? endpoint.path)"
        case let .emptyResponse(endpoint): return "No data returned back for endpoint \(endpoint.name ?? endpoint.path)"
        case let .dataCorrupted(endpoint): return "The returned data is corrupted for endpoint \(endpoint.name ?? endpoint.path)"
        case let .invalidResponse(response): return "Server returned invalid response type: \(response?.description ??  "nil")"
        case let .unknownStatusCode(statusCode): return "Server returned unknown response: \(statusCode)"
        }
    }
}
