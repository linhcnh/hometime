//
//  Direction.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A direction enum containing north and south in which each returns different title and stop id
enum Direction: CaseIterable {
    case north
    case south

    /// Title associated with the direction
    var title: String {
        switch self {
        case .north: return "North"
        case .south: return "South"
        }
    }

    /// Stop id associated with the direction
    var stopId: String {
        switch self {
        case .north: return "4055"
        case .south: return "4155"
        }
    }
}
