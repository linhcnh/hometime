//
//  APIClient.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// A dictionary of parameters to apply to a `URLRequest`.
typealias Parameters = [String : String]

/// A dictionary of headers to apply to a `URLRequest`.
typealias HTTPHeaders = [String : String]

/// An enum with all standard http methods
enum HTTPMethod: String {
    case options
    case get
    case head
    case post
    case put
    case patch
    case delete
    case trace
    case connect
}
