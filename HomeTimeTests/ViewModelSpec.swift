//
//  ViewModelSpec.swift
//  HomeTimeTests
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import HomeTime

class ViewModelSpec: QuickSpec {
    override func spec() {
        describe("ViewModel") {
            let tokenJson = MockTokenResponse.all.mock
            let tokenResponseModel = TokenResponseModel(json: tokenJson)!
            let tramJson = MockTramResponse.all.mock
            let tramResponseModel = TramResponseModel(json: tramJson)!
            let stopId = Direction.north.stopId

            let mockEndpointResponseObjects = [Endpoint.token.url!: tokenResponseModel,
                                               Endpoint.trams(stopId: stopId).url!: tramResponseModel] as [URL : Any]


            let networkService = MockNetworkService(mockEndpointResponseObjects: mockEndpointResponseObjects)
            let authManager = AuthManager(networkService: networkService)
            let dateConverter = DotNetDateConverter()
            var viewModel: ViewModel!

            beforeEach {
                viewModel = ViewModel(networkService: networkService,
                                      authManager: authManager,
                                      dateConverter: dateConverter)
            }

            context("when initialising the view model") {
                it("should match the cell identifier") {
                    expect(viewModel.cellIdentifier).to(equal("TramCellIdentifier"))
                    expect(viewModel.alertActionTitle).to(equal("OK"))
                    expect(viewModel.alertControllerTitle).to(equal("Error"))
                }

                it("should match section header titles") {
                    expect(viewModel.titleForItems(at: 0)).to(equal("North"))
                    expect(viewModel.titleForItems(at: 1)).to(equal("South"))
                }

                it("should have no tram data") {
                    let fistIndexPath = IndexPath(item: 0, section: 0)

                    expect(viewModel.timetable.count).to(equal(0))
                    expect(viewModel.tramsForItems(at: 0)).to(beNil())
                    expect(viewModel.tramInfoForItem(at: fistIndexPath)).to(equal("No upcoming trams. Tap load to fetch"))
                }
            }

            context("when making tram requests") {
                it("should match tram data") {
                    viewModel.getTrams(completion: nil)

                    let fistIndexPath = IndexPath(item: 0, section: 0)
                    let fromDate = dateConverter.dateFromDotNetFormattedDateString("/Date(1574830300000+1100)/")!

                    expect(viewModel.timetable.count).to(equal(1))
                    expect(viewModel.tramsForItems(at: 0)?.count).to(equal(3))
                    expect(viewModel.tramInfoForItem(at: fistIndexPath, with: fromDate)).to(equal("Route 78 to Balaclava at 15:55 (3 mins)"))
                }

                it("should have no tram data after clear") {
                    viewModel.getTrams(completion: nil)
                    viewModel.clearTimetable()

                    let fistIndexPath = IndexPath(item: 0, section: 0)

                    expect(viewModel.timetable.count).to(equal(0))
                    expect(viewModel.tramsForItems(at: 0)).to(beNil())
                    expect(viewModel.tramInfoForItem(at: fistIndexPath)).to(equal("No upcoming trams. Tap load to fetch"))
                }
            }
        }
    }
}
