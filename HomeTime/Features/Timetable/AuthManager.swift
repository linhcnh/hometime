//
//  AuthManager.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import Foundation

/// An authentication manager which manage api tokens, ids and dev info
class AuthManager {
    private let networkService: NetworkServiceProtocol

    let aid = "TTIOSJSON"
    let devInfo = "HomeTimeiOS"
    let cid = "2"

    var tkn: String?

    /// To init an authentication manager
    /// - Parameter networkService: a network service instance injected
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }

    /// To make a token request
    /// - Parameter success: return and assign a token to `tkn` variable
    /// - Parameter failure: return an error
    func requestToken(success: ((String) -> Void)?, failure: ((APIClientError) -> Void)?) {
        let parameters = ["aid": aid, "devInfo": devInfo]

        let successCompletion: (TokenResponseModel) -> Void = { [weak self] model in
            guard model.hasError == false else {
                failure?(APIClientError.general(message: model.errorMessage))
                return
            }

            guard let tokenModel = model.responseObject.first else {
                failure?(APIClientError.dataCorrupted(endpoint: Endpoint.token))
                return
            }

            self?.tkn = tokenModel.deviceToken
            success?(tokenModel.deviceToken)
        }

        networkService.performRequest(endpoint: Endpoint.token,
                                      parameters: parameters,
                                      success: successCompletion,
                                      failure: failure)
    }
}
