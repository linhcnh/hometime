//
//  Copyright (c) 2015 REA. All rights reserved.
//

import Foundation

/// Handy class to convert and formate dates
class DotNetDateConverter {
    /// Calculate the time distance by minutes between 2 dates
    /// - Parameter from: from date to be calculated
    /// - Parameter to: to date to be calculated
    private func minutesDistance(from: Date, to: Date) -> Int? {
        return Calendar.current.dateComponents([.minute], from: from, to: to).minute
    }

    /// Convert date string from .NET format to a standard date
    /// - Parameter string: a  .NET format date string
    func dateFromDotNetFormattedDateString(_ string: String) -> Date? {
        guard let startRange = string.range(of: "("),
          let endRange = string.range(of: "+") else { return nil }

        let lowBound = string.index(startRange.lowerBound, offsetBy: 1)
        let range = lowBound..<endRange.lowerBound

        let dateAsString = string[range]
        guard let time = Double(dateAsString) else { return nil }
        let unixTimeInterval = time / 1000
        return Date(timeIntervalSince1970: unixTimeInterval)
    }

    /// Convert a date string from .NET format to the standard format + add calculated minutes
    /// - Parameter string: a  .NET format date string
    /// - Parameter from: from date to calculate the distance by minute, it is current by default
    func formattedDateFromString(_ string: String, distanceFrom from: Date) -> String {
        guard let date = dateFromDotNetFormattedDateString(string),
            let minutes = minutesDistance(from: from, to: date) else { return "" }

        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return "\(formatter.string(from: date)) (\(minutes) mins)"
    }
}
